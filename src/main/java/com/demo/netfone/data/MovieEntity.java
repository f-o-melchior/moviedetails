package com.demo.netfone.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieEntity implements Serializable {
	private static final long serialVersionUID = -3578885672379354988L;

	@JsonProperty("Title")
	private String title;
	@JsonProperty("Year")
	private Integer year;
	@JsonProperty("Director")
	private String director;
	
	public MovieEntity() {

	}

	public MovieEntity(String title, Integer year, String director) {
		this.title = title;
		this.year = year;
		this.director = director;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public String toString() {
		return "MovieRequestEntity [title=" + title + ", year=" + year + ", director=" + director + "]";
	}
}
