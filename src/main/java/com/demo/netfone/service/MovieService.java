package com.demo.netfone.service;

import com.demo.netfone.data.MovieEntity;

public abstract class MovieService {
	
	//OMDB API KEY
	// Please generate one if this key is not working or change the value of the api key
	// Generate here: https://www.omdbapi.com/apikey.aspx
	private static final String OMDB_API_KEY = "9330546b";
	
	//TMDB API KEY
	// Please generate one if this key is not working or change the value of the api key
	// - i could not generate on the website - 
	private static final String TMDB_API_KEY = "";
	
	//OMDB API
	private static final String OMDB_API_URL = "http://www.omdbapi.com/";
	private static final String OMDB_TITLE_POSTFIX = "?t=";
	private static final String OMDB_API_KEY_POSTFIX = "&apikey=";

	
	//THE MOVIE DB API
	private static final String TMDB_API_URL = "https://www.themoviedb.org/3/search/movie";
	private static final String TMDB_API_KEY_POSTFIX = "?api_key=";
	private static final String TMDB_QUERY_POSTFIX = "&query=";
	private static final String TMDB_INCLUDE_ADULT = "&include_adult=true";
	
	protected String getOmdbURL(String title) {
		return OMDB_API_URL + OMDB_TITLE_POSTFIX + title + OMDB_API_KEY_POSTFIX + OMDB_API_KEY;
	};
	
	protected String getTmdbURL(String title) {
		return TMDB_API_URL + TMDB_API_KEY_POSTFIX + TMDB_API_KEY + TMDB_QUERY_POSTFIX + title + TMDB_INCLUDE_ADULT;
	};
	
	protected boolean validateResponse(MovieEntity responseResult) {
		
		if(null == responseResult.getTitle() || responseResult.getTitle().isEmpty()) {
			return false;
		}
		
		if(null == responseResult.getDirector() || responseResult.getDirector().isEmpty()) {
			return false;
		}
		
		if(null == responseResult.getYear() || responseResult.getYear() == 0) {
			return false;
		}
		
		return true;
	};
}
