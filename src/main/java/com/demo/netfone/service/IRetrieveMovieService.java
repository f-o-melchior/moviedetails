package com.demo.netfone.service;

import com.demo.netfone.data.MovieEntity;
import com.demo.netfone.util.MovieServiceException;
import com.demo.netfone.util.WebApiType;

public interface IRetrieveMovieService {

	MovieEntity retrieveMovies(String title, WebApiType webApiType) throws MovieServiceException;
}
