package com.demo.netfone.service.core;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.netfone.data.MovieEntity;
import com.demo.netfone.service.IRetrieveMovieService;
import com.demo.netfone.service.MovieService;
import com.demo.netfone.util.MovieServiceException;
import com.demo.netfone.util.WebApiType;

@Service
public class RetrieveMovieServiceImpl extends MovieService implements IRetrieveMovieService {

	@Override
	public MovieEntity retrieveMovies(String title, WebApiType apiType) throws MovieServiceException {
		if (apiType == WebApiType.OMDB) {
			return getMovieResponse(this.getOmdbURL(title));
		} else {
			return getMovieResponse(this.getTmdbURL(title));
		}
	}

	private MovieEntity getMovieResponse(String url) throws MovieServiceException {
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<MovieEntity> response = restTemplate.exchange(
				url,
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<MovieEntity>() {
				});

		if (HttpStatus.OK == response.getStatusCode()) {
			MovieEntity responseResult = response.getBody();

			if (this.validateResponse(responseResult)) {
				return responseResult;
			} else {
				throw new MovieServiceException("No movies found in database api!");
			}
		} else {
			throw new MovieServiceException("Can not get movies from remote db-s... Please make sure "
					+ "you are using the correct apikeys!");
		}
	}
}
