package com.demo.netfone.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import com.demo.netfone.data.MovieEntity;
import com.demo.netfone.service.IRetrieveMovieService;
import com.demo.netfone.util.MovieServiceException;
import com.demo.netfone.util.WebApiType;

@RestController
@RequestMapping("/movies")
public class MovieController {
	
	@Autowired
	private IRetrieveMovieService movieService;

	@GetMapping("/{movieTitle}")
	public MovieEntity retrieveMovies(
			@PathVariable(value = "movieTitle") final String title,
			@RequestParam("api") final String apiName
			) {
		try {
			return movieService.retrieveMovies(title, WebApiType.findByName(apiName));
		} catch (MovieServiceException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
		} catch (HttpClientErrorException ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request sent from the movie tool...", ex);
		}
	}
}
