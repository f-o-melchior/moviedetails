package com.demo.netfone.util;

public enum WebApiType {
	
	OMDB("omdb"),
	TMDV("tmdb");
	
	private final String webApiName;

	private WebApiType(String webApiName) {
		this.webApiName = webApiName;
	}

	public String getWebApiName() {
		return webApiName;
	}
	
	public static WebApiType findByName(String value) {
		
		for (WebApiType trainingType : values()) {
			if (trainingType.webApiName.equals(value)) {
				return trainingType;
			}
		}
		throw new IllegalArgumentException("Unsupported training type: " + value);
	}

}
