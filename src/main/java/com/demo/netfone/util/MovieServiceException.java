package com.demo.netfone.util;

public class MovieServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public MovieServiceException(String errorMessage) {
        super(errorMessage);
    }
}