package com.demo.netfone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetfoneDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NetfoneDemoApplication.class, args);
	}
}
